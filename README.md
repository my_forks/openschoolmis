<h1 align="center">网非教学管理平台</h1>
<p align="center">
    <a href="http://www.dnd8.com">
        <img src="https://img.shields.io/badge/OfficialWebsite-%E7%BD%91%E9%9D%9E%E7%A7%91%E6%8A%80-yellow.svg" />
    </a>
<a href="http://www.dnd8.com">
        <img src="https://img.shields.io/badge/Licence-GPL3.0-green.svg?style=flat" />
    </a>
    <a href="http://www.dnd8.com">
        <img src="https://img.shields.io/badge/Edition-0.1-blue.svg" />
    </a>
     <a href="https://gitee.com/wfdot/openschoolmis/repository/archive/master.zip">
        <img src="https://img.shields.io/badge/download-24.4m-red.svg" />
    </a>
    </p>
<p align="center">    
    <b>如果对您有帮助，麻烦点右上角 "Star" 支持一下，谢谢！</b>
</p>

#### 介绍
网非教学管理平台主要服务于国内的高中和大学，包括档案管理（学校、教师、学生、家长）、学籍管理、课程表、成绩管理 、学费管理，专业管理等功能，打通学校、教师、学生/家长的信息流，提升学校教育教学水平和管理能力。

#### 软件架构
使用PHP、MySQL开发，基于fastadmin框架。

### 导航栏目

[官网地址](http://dnd8.com/)
 | [详细介绍](http://dnd8.com/product/4)
 | [授权价格](http://dnd8.com/product/4)
- - -

### :tw-1f427: QQ交流群
 网非QQ开发群：994690164<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=97f23179a3ea43dd17ed827678dfad03aab5bb9b0275a1195495a749db5ccfee"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="网非开发群" title="网非开发群"></a>

 ###   :tw-1f50a: 开源版使用须知
1. 允许用于个人学习、毕业设计、教学案例、公益事业;
2. 如果商用必须保留版权信息，请自觉遵守。开源版不适合商用，商用请购买商业版;
3. 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负。

### 商业版与 :tw-1f19a: 开源版差异
开源版 | 专业版 | 企业版
:-: | :-: | :-:
测试版本 | 稳定版本 | 稳定版本
普通群 | VIP群 | 一对一服务
无安装服务 | 有安装服务：帮助开发者把系统搭建起来，编译成功并可正常运行 | 有安装服务：帮助开发者把系统搭建起来，编译成功并可正常运行
无开发支持服务 | 无开发支持服务 | 有开发支持服务：提供一个月开发支持服务，工作时间在线上提供开发支持，讲解现有系统的架构、数据库结构和代码
无商业授权 | 有商业授权：可以在1个IP地址商用，不可出售代码和资源 | 有商业授权：不限制商用IP地址数，不可出售代码和资源

![网非代码商城官方小程序购买商业版](/readme/image/wfshop.jpg "wfshop.jpg")

### 页面展示
![demo](/readme/image/demo.jpg "demo.jpg")

### 参与开发

请提交 [网非教学管理平台](https://gitee.com/wfdot/openschoolmis/pulls)。


### 版权信息

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2017-2022 by 北京网非科技有限公司 (http://www.dnd8.com)

All rights reserved。


### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
