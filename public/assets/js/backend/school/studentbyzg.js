define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'school/studentbyzg/index' + location.search,
                    // add_url: 'school/studentbyzg/add',
                    edit_url: 'school/studentbyzg/edit',
                    // del_url: 'school/studentbyzg/del',
                    multi_url: 'school/studentbyzg/multi', 
                    table: 'student',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'student_number', title: __('Student_number')},
                        // {field: 'work_station_dict', title: __('Work_station_dict'), searchList: Config.workStationDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        // {field: 'exametime', title: __('Exametime')},
                        {field: 'major_level_dict', title: __('Major_level_dict'), searchList: Config.majorLevelDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        {field: 'major_id', title: __('Major_id'),searchList: $.getJSON("school/major/searchlist"),visible:false},//只用于查询（不显示列表），查询是where必须是字段名.admin\controller\school
                        {field: 'major_id_text', title: __('Major_id'),operate:false},//只用于展示，不显示在条件查询中
                        {field: 'major_id_lev', title: "级别",operate:false},
                        // {field: 'student_type_dict', title: __('Student_type_dict'), searchList: Config.studentTypeDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        // {field: 'student_id_code', title: __('Student_id_code')},
                        // {field: 'student_sex', title: __('Student_sex'), searchList: {"0":__('Student_sex 0'),"1":__('Student_sex 1')}, formatter: Table.api.formatter.normal},
                        // {field: 'student_birthday', title: __('Student_birthday'), operate:'RANGE', addclass:'datetimerange'},
                        // {field: 'student_nation', title: __('Student_nation')},
                        // {field: 'student_birth_localtion', title: __('Student_birth_localtion')},
                        // {field: 'student_political_status_dict', title: __('Student_political_status_dict'), searchList: Config.studentPoliticalStatusDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        // {field: 'student_mail_address', title: __('Student_mail_address')},
                        // {field: 'student_postcode', title: __('Student_postcode')},
                        // {field: 'student_tel', title: __('Student_tel')},
                        // {field: 'studentimage', title: __('Studentimage'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        // {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1'),"2":__('Status 2'),"3":__('Status 3')}, formatter: Table.api.formatter.status},
                        // {field: 'student_login_pwd', title: __('Student_login_pwd')},
                        // {field: 'student_last_level_dict', title: __('Student_last_level_dict'), searchList: Config.studentLastLevelDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        // {field: 'student_last_school', title: __('Student_last_school')},
                        // {field: 'student_work_unit', title: __('Student_work_unit')},
                        // {field: 'student_work_unit_tel', title: __('Student_work_unit_tel')},
                        // {field: 'scoure_politics', title: __('Scoure_politics'), operate:'BETWEEN'},
                        // {field: 'scoure_chinese', title: __('Scoure_chinese'), operate:'BETWEEN'},
                        // {field: 'scoure_math', title: __('Scoure_math'), operate:'BETWEEN'},
                        // {field: 'scoure_english', title: __('Scoure_english'), operate:'BETWEEN'},
                        // {field: 'scoure_add', title: __('Scoure_add'), operate:'BETWEEN'},
                        // {field: 'scoure_sum', title: __('Scoure_sum'), operate:'BETWEEN'},
                        // {field: 'student_last_type_dict', title: __('Student_last_type_dict'), searchList: Config.studentLastTypeDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        // {field: 'student_last_starttime', title: __('Student_last_starttime'), operate:'RANGE', addclass:'datetimerange'},
                        // {field: 'student_last_endtime', title: __('Student_last_endtime'), operate:'RANGE', addclass:'datetimerange'},
                        // {field: 'student_remark', title: __('Student_remark')},
                        // {field: 'create_by', title: __('Create_by')},
                        // {field: 'update_by', title: __('Update_by')},
                        // {field: 'remark', title: __('Remark')},
                        // {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        // {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'academic_record', title: __('Academic_record'), operate:'BETWEEN'},
                        {field: 'by_status', title: __('By_status'), searchList: {"0":__('By_status 0'),"1":__('By_status 1')}, formatter: Table.api.formatter.status},
                        {field: 'fee_status', title: __('Fee_status'), searchList: {"0":__('Fee_status 0'),"1":__('Fee_status 1')}, formatter: Table.api.formatter.status},
                        {field: 'cj_status', title: __('Cj_status'), searchList: {"0":__('Cj_status 0'),"1":__('Cj_status 1')}, formatter: Table.api.formatter.status},
                        {field: 'lw_status', title: __('Lw_status'), searchList: {"0":__('Lw_status 0'),"1":__('Lw_status 1')}, formatter: Table.api.formatter.status},
                        {field: 'diploma_no', title: __('Diploma_no')},
                        {field: 'jy_status', title: __('Jy_status'), searchList: {"0":__('Jy_status 0'),"1":__('Jy_status 1')}, formatter: Table.api.formatter.status},
                        {field: 'certificate_no', title: __('Certificate_no')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(".btn-szjf").on("click",function(){
                var ids = $('#table').bootstrapTable('getSelections', null);
                var idstext = '';
                if (ids.length == 0) {
                    idstext = '';
                } else {
                ids.forEach(element => {
                    idstext += element.id + ",";
                });
                }
                idstext += "0";
                Fast.api.ajax({
                    url: "school/studentbyzg/szjf?ids=" + idstext,
                    //data: {file: data.url},
                }, function (data, ret) {
                    table.bootstrapTable('refresh');
                });
            });

        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'school/studentbyzg/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), align: 'left'},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'school/studentbyzg/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'school/studentbyzg/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});