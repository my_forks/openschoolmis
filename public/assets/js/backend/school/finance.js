define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'school/finance/index' + location.search,
                    add_url: 'school/finance/add',
                    edit_url: 'school/finance/edit',
                    // del_url: 'school/finance/del',
                    multi_url: 'school/finance/multi',
                    table: 'finance',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                queryParams : function (params) {
                    var filter = JSON.parse(params.filter);
                    var op = JSON.parse(params.op);
                    filter['s.student_number'] = filter.student_id_text;
                    delete filter.student_id_text;
                    op['s.student_number'] = op.student_id_text;
                    params.filter = JSON.stringify(filter);
                    params.op = JSON.stringify(op);
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), visible:false, operate: false},
                        {field: 'student_id_text', title: __('Student_id'), operate:"LIKE"},
                        {field: 'student_name', title: __('Student_name'), operate:"LIKE"},
                        {field: 'major_id', title: __('Major_id'),searchList: $.getJSON("school/major/searchlist"),visible:false},//只用于查询（不显示列表），查询是where必须是字段名.admin\controller\school
                        {field: 'major_id_text', title: __('Major_id'),operate:false},//只用于展示，不显示在条件查询中
                        {field: 'major_level_dict', title: __('Major_level_dict'), searchList: Config.majorLevelDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        {field: 'level', title: __('Level')},
                        {field: 'term_dict', title: __('Term_dict'), searchList: Config.termDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label,visible:false,operate:false},
                        {field: 'remark', title: "学年", operate:"LIKE"},
                        {field: 'sex', title: __('Sex'), searchList: {"0":__('Sex 0'),"1":__('Sex 1')}, formatter: Table.api.formatter.normal,operate:false},
                        {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1')}, formatter: Table.api.formatter.status},
                        {field: 'money', title: __('Money'), operate:false, visible:false}, 
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime,visible:false},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime,visible:false,operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(".btn-szjf").on("click",function(){
                var ids = $('#table').bootstrapTable('getSelections', null);
                var idstext = '';
                if (ids.length == 0) {
                    idstext = '';
                } else {
                    ids.forEach(element => {
                        idstext += element.id + ",";
                    });
                }
                idstext += "0";
                Fast.api.ajax({
                    url: "school/studentbyzg/szjf?ids=" + idstext,
                    //data: {file: data.url},
                }, function (data, ret) {
                    table.bootstrapTable('refresh');
                });
            });

        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'school/finance/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'school/finance/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'school/finance/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () { 
                require(['selectpage'], function () {
                    $('#c-student_id').selectPage({
                        eAjaxSuccess: function (data) {
    
                            data.list = typeof data.rows !== 'undefined' ? data.rows : (typeof data.list !== 'undefined' ? data.list : []);
                            data.totalRow = typeof data.total !== 'undefined' ? data.total : (typeof data.totalRow !== 'undefined' ? data.totalRow : data.list.length);
                            return data;
    
                        },
                        eSelect: function (data) {
                            $("#c-student_name").val(data.name);
                            $("#c-student_id_code").val(data.student_id_code);
                            $("#c-major_level_dict").val(data.major_level_dict).selectpicker('render');
                            $("#c-major_id").val(data.major_id).selectPageRefresh();
                            $("#c-level").val(data.level);
                            $("#c-sex").val(data.student_sex).selectpicker('render');
                        }
                    });
                });
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});