<?php

return [
    'Id'                => '专业id',
    'Name'              => '专业名',
    'Major_level_dict'  => '培养层次',
    'Years'             => '培养年限',
    'Lesson_ids'        => '课程信息',
    'Work_station_dict' => '工作站',
    'Fee'               => '学费',
    'Level'             => '级别',
    'Status'            => '状态',
    'Status 0'          => '注销',
    'Status 1'          => '正常',
    'Create_by'         => '创建者',
    'Createtime'        => '创建时间',
    'Updatetime'        => '更新时间',
    'Remark'            => '备注信息',
    'Deletetime'        => '删除时间'
];
