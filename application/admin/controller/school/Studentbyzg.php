<?php

namespace app\admin\controller\school;

use think\Db;
use think\Exception;
use app\common\controller\Backend;
/**
 * 毕业资格审核
 *
 * @icon fa fa-circle-o
 */
class Studentbyzg extends Backend
{
    
    /**
     * Studentbyzg模型对象
     * @var \app\admin\model\school\Studentbyzg
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\school\Studentbyzg;
        $this->view->assign("workStationDictList", $this->model->getWorkStationDictList());
        $this->assignconfig("workStationDictList", $this->model->getWorkStationDictList());
        $this->view->assign("majorLevelDictList", $this->model->getMajorLevelDictList());
        $this->assignconfig("majorLevelDictList", $this->model->getMajorLevelDictList());
        $this->view->assign("studentTypeDictList", $this->model->getStudentTypeDictList());
        $this->assignconfig("studentTypeDictList", $this->model->getStudentTypeDictList());
        $this->view->assign("studentSexList", $this->model->getStudentSexList());
        $this->view->assign("studentPoliticalStatusDictList", $this->model->getStudentPoliticalStatusDictList());
        $this->assignconfig("studentPoliticalStatusDictList", $this->model->getStudentPoliticalStatusDictList());
        $this->view->assign("statusList", $this->model->getStatusList());
        $this->view->assign("studentLastLevelDictList", $this->model->getStudentLastLevelDictList());
        $this->assignconfig("studentLastLevelDictList", $this->model->getStudentLastLevelDictList());
        $this->view->assign("studentLastTypeDictList", $this->model->getStudentLastTypeDictList());
        $this->assignconfig("studentLastTypeDictList", $this->model->getStudentLastTypeDictList());
        $this->view->assign("feeStatusList", $this->model->getFeeStatusList());
        $this->view->assign("cjStatusList", $this->model->getCjStatusList());
        $this->view->assign("lwStatusList", $this->model->getLwStatusList());
        $this->view->assign("byStatusList", $this->model->getByStatusList());
        $this->view->assign("jyStatusList", $this->model->getJyStatusList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    
    /**
     * 一键设置已缴费
     */
    public function szjf($ids = "")
    {
        // update sc_student set fee_status = '1' where fee_status != '1' and id in (select distinct student_id from sc_finance where student_id not in  (select distinct student_id from sc_finance where status = '0') )
        try{
            //$res = Db::execute("update sc_student set fee_status = '1' where fee_status != '1' and id in (select distinct student_id from sc_finance where student_id not in  (select distinct student_id from sc_finance where status = '0') )");
            if ($ids == "0"){
                $res1 = Db::execute("update sc_finance set status = '1' where status != '1' ");
            }else{
                $res1 = Db::execute("update sc_finance set status = '1' where status != '1' and id in (".$ids.")");
            }
            $this->success("根据学员实际缴费情况，一键设置已缴费成功");
            // return "success";
        } catch (Exception $e) {
            $msg = $e->getMessage();
            $this->error($msg); 
            // return  "error";
        } 
    }

}
