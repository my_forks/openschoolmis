<?php

namespace app\admin\model\school;

use think\Model;
use think\Config;
use traits\model\SoftDelete;

class MajorLesson extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'major_lesson';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'major_level_dict_text',
        'class_time_dict_text',
        'lesson_id_text',
        'major_id_text',
        'admin_ids_text',
        'room_dict_text'
    ];
    

    
    public function getMajorLevelDictList()
    {
        return Config::get("site.major_level");
    }

    public function getClassTimeDictList()
    {
        return Config::get("site.class_time");
    }

    public function getRoomDictList()
    {
        return Config::get("site.room");
    }


    public function getMajorLevelDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['major_level_dict']) ? $data['major_level_dict'] : '');
        $list = $this->getMajorLevelDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getClassTimeDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['class_time_dict']) ? $data['class_time_dict'] : '');
        $list = $this->getClassTimeDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getRoomDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['room_dict']) ? $data['room_dict'] : '');
        $list = $this->getRoomDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getLessonIdTextAttr($value, $data)
    { 
        $lessons = (new Lesson())->field("name")->where("id", $data['lesson_id'])->find();

        return  $lessons['name'];
    }

    public function getMajorIdTextAttr($value, $data)
    { 
        $major = (new Major())->field("name")->where("id", $data['major_id'])->find();

        return  $major['name'];
    }

    public function getAdminIdsTextAttr($value, $data)
    { 
        $admins = model("Admin")->field("nickname")->where("id", "in", $data['admin_ids'])->select();
        return implode(",", array_column($admins, 'nickname'));
    }
    //根据专业和课程id获取组合信息
    public function getMvL($majorid, $lessonid)
    { 
        $info = $this->where("major_id",  $majorid)->where("lesson_id",$lessonid)->select();
        return $info;
    }




}
